import pandas as pd
import pyarrow.csv as csv
import plotly.express as px
import plotly.offline as pyo
import plotly.graph_objects as go

import dash
import dash_core_components as dcc
import dash_html_components as html

file_path1 = '/Users/Mikolaj/Desktop/Coding/Dash_app/data/stan_liczebnym_chusa.txt'
file_path2 = '/Users/Mikolaj/Desktop/Coding/Dash_app/data/Statystyki instruktorow - Sheet5.csv'
file_path3 = '/Users/Mikolaj/Desktop/Coding/Dash_app/data/Statystyki instruktorow - Mianowanych Instrukorow.csv'
file_path4 = '/Users/Mikolaj/Desktop/Coding/Dash_app/data/Statystyki instruktorow - Stan Hufcow.csv'
file_path5 = '/Users/Mikolaj/Desktop/Coding/Dash_app/data/Statystyki_011521 - Sheet4.csv'

df1 = csv.read_csv(file_path1).to_pandas()
df2 = csv.read_csv(file_path2).to_pandas()
df3 = csv.read_csv(file_path3).to_pandas()
df4 = csv.read_csv(file_path4).to_pandas()
df6 = csv.read_csv(file_path5).to_pandas()


df1['Uczestnicy'] = df1['Zuchy'] + df1['Harcerze'] + df1['Wędrownicy']
df1['Uczestnicy:Instruktor'] = df1['Uczestnicy']/df1['Instruktorzy']


fig00 = px.line(df1, x="Rok", y=["Zuchy", "Harcerze", "Wędrownicy", "Instruktorzy"], 
                title='Stan Chorągwi 1981-2020')
fig00.layout.yaxis['title']['text'] = 'Stan'
fig00.add_vline(x=1982, line_dash="dot", annotation_text="Zlot '82")
fig00.add_vline(x=1988, line_dash="dot", annotation_text="Zlot '88")
fig00.add_vline(x=1994, line_dash="dot", annotation_text="Zlot '94")
fig00.add_vline(x=2000, line_dash="dot", annotation_text="Zlot '00")
fig00.add_vline(x=2006, line_dash="dot", annotation_text="Zlot '06")
fig00.add_vline(x=2010, line_dash="dot", annotation_text="Zlot '10")
fig00.add_vline(x=2017, line_dash="dot", annotation_text="Zlot '17")


fig01 = px.line(df1, x="Rok", y="Harcerze", title='Stan Chorągwi: Harcerze')
fig01.update_traces(line_color='#EF553B')
fig01.add_hline(y=274, line_dash="dot", annotation_text="Średnia 274 Harcerzy", annotation_position="bottom left")

fig02 = px.line(df1, x="Rok", y="Zuchy", title='Stan Chorągwi: Zuchy')
fig02.add_hline(y=221, line_dash="dot", annotation_text="Średnia 221 Zuchy", annotation_position="bottom left")

fig03 = px.line(df1, x="Rok", y="Uczestnicy", title='Stan Chorągwi: Zuchy + Harcerze + Wędrownicy')
fig03.update_traces(line_color='#000000')
fig03.add_hline(y=559, line_dash="dot", annotation_text="Średnia 559 Uczestników", annotation_position="bottom left")

fig04 = px.line(df1, x="Rok", y="Wędrownicy", title='Stan Chorągwi: Wędrownicy')
fig04.update_traces(line_color='#00CC96')
fig04.add_hline(y=64, line_dash="dot", annotation_text="Średnia 64 Wędrowników", annotation_position="bottom left")

fig05 = px.line(df1, x="Rok", y="Instruktorzy", title='Stan Chorągwi: Instruktorzy')
fig05.update_traces(line_color='#AB63FA')
fig05.add_hline(y=100, line_dash="dot", annotation_text="Średnia 100 Instruktorów", annotation_position="bottom left")


krakow = df4['Hufiec ']=='Krakow'
fig001 = px.line(df4[krakow], x='Rok', y=['Zuchy', 'Harcerze', 'Wedrownicy'], title='Stan Hufiec Kraków')
fig001.layout.yaxis['title']['text'] = 'Stan'

kresy = df4['Hufiec ']=='Kresy'
fig002 = px.line(df4[kresy], x='Rok', y=['Zuchy', 'Harcerze', 'Wedrownicy'], title='Stan Hufiec Kresy')
fig002.layout.yaxis['title']['text'] = 'Stan'

warmia = df4['Hufiec ']=='Warmia'
fig003 = px.line(df4[warmia], x='Rok', y=['Zuchy', 'Harcerze', 'Wedrownicy'], title='Stan Hufiec Warmia')
fig003.layout.yaxis['title']['text'] = 'Stan'

warta = df4['Hufiec ']=='Warta'
fig004 = px.line(df4[warta], x='Rok', y=['Zuchy', 'Harcerze', 'Wedrownicy'], title='Stan Hufiec Warta')
fig004.layout.yaxis['title']['text'] = 'Stan'


fig = go.Figure(data=[go.Candlestick(x=df2['Rok'],
                open=df2['Open'],
                high=df2['High'],
                low=df2['Low'],
                close=df2['Close'])])

fig.update_layout(title='Stan Chorągwi: Instruktorzy', xaxis_rangeslider_visible=False)


bar1 = go.Bar(x=df3['Rok'], y=df3['pwd'], name='PWD', marker={'color': '#000080'})
bar2 = go.Bar(x=df3['Rok'], y=df3['phm'], name='PHM', marker={'color': '#008000'})
bar3 = go.Bar(x=df3['Rok'], y=df3['hm'], name='HM', marker={'color': '#FF0000'})

data = [bar1, bar2, bar3]
layout = go.Layout(title='Mianowanych Instruktorów Według Stopni')
fig2 = go.Figure(data=data, layout=layout)


fig4 = px.line(df1, x="Rok", y="Uczestnicy:Instruktor", title='Stosunek Uczestników:Instruktora')

df2['Uczestnicy'] = df1['Uczestnicy'].loc[19:][::-1].reset_index()['Uczestnicy']
df2['Uczestników:Instruktorów'] = df2['Uczestnicy']/df2['Close']

fig5 = px.line(df2, x="Rok", y='Uczestników:Instruktorów', title='Stosunek Uczestników:Instruktora')

bar01 = go.Bar(x=df6['Rok'], y=df6['pwd'], name='PWD', marker={'color': '#000080'})
bar02 = go.Bar(x=df6['Rok'], y=df6['phm'], name='PHM', marker={'color': '#008000'})
bar03 = go.Bar(x=df6['Rok'], y=df6['hm'], name='HM', marker={'color': '#FF0000'})

data6 = [bar01, bar02, bar03]
layout6 = go.Layout(title='Rejestrowanych Instruktorów Według Stopni 2013+')
fig6 = go.Figure(data=data6, layout=layout6)

app = dash.Dash()
server = app.server

app.layout = html.Div([dcc.Graph(id='chusa_stan', figure=fig00),
                       dcc.Graph(id='chusa_harc', figure=fig01),
                       dcc.Graph(id='chusa_zuchy', figure=fig02),
                       dcc.Graph(id='chusa_wedros', figure=fig04),
                       dcc.Graph(id='chusa_inst', figure=fig05),
                       dcc.Graph(id='chusa_uczestnicy', figure=fig03),
                       dcc.Graph(id='krakow', figure=fig001),
                       dcc.Graph(id='kresy', figure=fig002),
                       dcc.Graph(id='warmia', figure=fig003),
                       dcc.Graph(id='warta', figure=fig004),
                       dcc.Graph(id='bar_graph', figure=fig2),
                       dcc.Graph(id='candlestick_chart', figure=fig),
                       dcc.Graph(id='ratio_graph', figure=fig5),
                       dcc.Graph(id='rejs_graph', figure=fig6)])

if __name__ == '__main__':
    app.run_server()